﻿all:
	gcc -g -Wall -o quick_sort quick_sort.c -fopenmp
	gcc -g -Wall -o quick_sort_part quick_sort_part.c -fopenmp
